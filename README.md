### Transfer Money API

Allows to transfer money between accounts, test accounts:
- testSourceAccount with $100usd
- testDestinationAccount with $100usd

### How to start

#### Gradle
````
./gradlew run
````

#### Docker
````
./gradlew assemble
docker build . -t revolut-demo
docker run --rm -p 8080:8080 revolut-demo
````
 
````
curl http://localhost:8080/v1/accounts/testSourceAccount

````

### Endpoint doc

POST on /v1/transfer
```
{
	"sourceAccount": "testSourceAccount",
	"destinationAccount": "testDestinationAccount",
	"amount": "10",
	"currencyCode": "USD"
}
```

GET on /v1/accounts/{accountId}
````
{"accountId":"testSourceAccount","amount":100,"currencyCode":"USD"}
```

### Tech Stack
- Java 8 
- [micronaut](https://micronaut.io/) as supporting framework for this challenge as it's a lightweight framework
- H2 as in-memory db 
- Moneta for money handling operations
- MyBatis for persistence
- Flyway for db migrations
- Netty as webserver

### Assumptions
- Accounts should already be created
- Decided to use pessimistic locking using ``select for update`` the downside is that it blocks reads and writes on a row level
depending on the situation it could be a bottleneck problem specially if there are a lot of reads if there is no need to 
have consistent reads a replica can be used to mitigate the issue.
- Didn't change Lock timeout setting from [H2](http://www.h2database.com/html/advanced.html#mvcc)
- Not using any reactive features for the time being

### Improvements
- @Injection is not propagated by lombok (had to manually specify constructors otherwise the inject annotation would not work)
- The transaction manager implementation is super naive as the SQLSession has to be carried over to the repositories
- Perhaps using Jooq/JPA would have been a bit cleaner? 
- For production ready we would need a different DB
- Didn't use hexagonal architecture because it seems an overkill for small projects
- Work on TODO items
- Swagger annotations