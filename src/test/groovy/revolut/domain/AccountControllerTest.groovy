package revolut.domain


import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.RxHttpClient
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.test.annotation.MicronautTest
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification

import javax.inject.Inject

@MicronautTest
class AccountControllerTest extends Specification {

    @Inject
    @AutoCleanup
    @Shared
    EmbeddedServer embeddedServer

    @AutoCleanup
    RxHttpClient client

    void setup() {
        client = embeddedServer.applicationContext.createBean(RxHttpClient, embeddedServer.getURL())
    }


    void "should return account details"() {
        given:
        def accountId = "testSourceAccount"

        when:
        HttpResponse response = accountDetails(accountId)

        then:
        response.status == HttpStatus.OK
        response.body()
    }

    def accountDetails(accountId) {
        HttpRequest request = HttpRequest.GET("/v1/accounts/${accountId}")
        return getClient().toBlocking().exchange(request, String)
    }
}
