package revolut.domain


import io.micronaut.test.annotation.MicronautTest
import spock.lang.Specification

import javax.inject.Inject

import static revolut.provisioning.TransferMoneyCommandProvisioning.transferMoney

@MicronautTest
class TransferServiceTest extends Specification {

    @Inject
    TransferService transferService

    @Inject
    AccountService accountService

    def "should transfer money successfully"() {
        when:
        def transferResult = transferService.transfer(transferMoney(new BigDecimal("5.50")))

        and:
        def detailsSource = accountService.accountDetails("testSourceAccount")
        def detailsDestination = accountService.accountDetails("testDestinationAccount")

        then:
        transferResult.transactionId
        detailsSource.amount == new BigDecimal("94.50")
        detailsDestination.amount == new BigDecimal("105.50")
        //TODO make sure ledger entries are written properly
    }
}
