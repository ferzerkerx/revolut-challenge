package revolut.domain

import revolut.domain.exception.BadRequestException
import revolut.domain.exception.InsufficientFundsException
import org.javamoney.moneta.Money
import spock.lang.Specification

class AccountTest extends Specification {
    void "should return balance properly"() {
        given:
        def account = account()

        when:
        def balance = account.balance

        then:
        balance.currency.currencyCode == "USD"
        balance.numberStripped == new BigDecimal("23.45")

    }

    void "should withDrawFunds successfully"() {
        given:
        def account = account()

        when:
        def newBalance = account.withDrawFunds(tenDollars()).balance

        then:
        newBalance.currency.currencyCode == "USD"
        newBalance.numberStripped == new BigDecimal("13.45")
    }

    void "should fail to withDrawFunds"() {
        given:
        def account = account()

        when:
        account.withDrawFunds(hundredDollars()).balance

        then:
        final InsufficientFundsException insufficientFundsException = thrown()
        insufficientFundsException.message == "Account someAccountId has insufficient funds for amount: USD 100"
    }

    void "should fail to withDrawFunds with different currencies"() {
        given:
        def account = account()

        when:
        account.withDrawFunds(tenEuros()).balance

        then:
        final BadRequestException badRequestException = thrown()
        badRequestException.message == "Accounts cannot have different currencies."
    }

    void "should AddFunds successfully "() {
        given:
        def account = account()

        when:
        def newBalance = account.addFunds(tenDollars()).balance

        then:
        newBalance.currency.currencyCode == "USD"
        newBalance.numberStripped == new BigDecimal("33.45")
    }

    void "should fail to add funds with different currencies "() {
        given:
        def account = account()

        when:
        account.addFunds(tenEuros()).balance

        then:
        final BadRequestException badRequestException = thrown()
        badRequestException.message == "Accounts cannot have different currencies."
    }

    def tenDollars() {
        return Money.of(new BigDecimal("10"), "USD")
    }

    def tenEuros() {
        return Money.of(new BigDecimal("10"), "EUR")
    }

    def hundredDollars() {
        return Money.of(new BigDecimal("100"), "USD")
    }

    def account() {
        return Account.builder()
                .accountId("someAccountId")
                .currencyCode("USD")
                .amount(new BigDecimal("23.45"))
                .build()
    }
}
