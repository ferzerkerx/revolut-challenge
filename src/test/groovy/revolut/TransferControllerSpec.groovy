package revolut

import revolut.domain.command.TransferMoneyCommand
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.test.annotation.MicronautTest
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification

import javax.inject.Inject

import static revolut.provisioning.TransferMoneyCommandProvisioning.transferMoney

@MicronautTest
class TransferControllerSpec extends Specification {

    @Inject
    @AutoCleanup
    @Shared
    EmbeddedServer embeddedServer

    @AutoCleanup
    RxHttpClient client

    void setup() {
        client = embeddedServer.applicationContext.createBean(RxHttpClient, embeddedServer.getURL())
    }


    void "should process transfer command successfully"() {
        given:
        def transferCommand = transferMoney(new BigDecimal("10.50"))

        when:
        HttpResponse response = transfer(transferCommand)

        then:
        response.status == HttpStatus.OK
        response.body()
    }

    void "should fail for invalid request"() {
        given:
        def transferCommand = transferMoney(new BigDecimal("-10.50"))

        when:
        transfer(transferCommand)

        then:
        final HttpClientResponseException clientResponseException = thrown()
        clientResponseException.status == HttpStatus.BAD_REQUEST
    }

    def transfer(TransferMoneyCommand transferMoneyCommand) {
        HttpRequest request = HttpRequest.POST("/v1/transfer", transferMoneyCommand)
        return getClient().toBlocking().exchange(request, String)
    }
}
