package revolut.provisioning;

import revolut.domain.command.TransferMoneyCommand;
import lombok.experimental.UtilityClass;

import java.math.BigDecimal;

@UtilityClass
public class TransferMoneyCommandProvisioning {

    public static TransferMoneyCommand transferMoney(BigDecimal amount) {
        return TransferMoneyCommand.builder()
                .sourceAccount("testSourceAccount")
                .destinationAccount("testDestinationAccount")
                .amount(amount)
                .currencyCode("USD")
                .build();
    }
}
