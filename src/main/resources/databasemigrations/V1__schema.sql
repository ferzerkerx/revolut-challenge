DROP TABLE IF EXISTS ACCOUNT;
DROP TABLE IF EXISTS LEDGER;

CREATE TABLE ACCOUNT (
  id BIGINT SERIAL PRIMARY KEY auto_increment,
  account_id VARCHAR(255) NOT NULL UNIQUE, --TODO add index, this could be the primary key
  amount number NOT NULL,
  currency_code CHAR(3) NOT NULL
);


CREATE TABLE LEDGER (
  id BIGINT SERIAL PRIMARY KEY auto_increment,
  account_id VARCHAR(255) NOT NULL, --TODO add index
  reference VARCHAR(255) NOT NULL,
  transaction_id VARCHAR(40) NOT NULL, --TODO add index
  amount number NOT NULL,
  currency_code CHAR(3) NOT NULL,
  operation VARCHAR(6) NOT NULL,  -- TODO this could be a boolean flag
  created_at TIMESTAMP WITH TIME ZONE NOT NULL
);