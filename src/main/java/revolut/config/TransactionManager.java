package revolut.config;

import revolut.domain.exception.DataAccessException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.function.Function;

//TODO this could be in an annotation and use it with an interceptor in the services
@Singleton
@Slf4j
public class TransactionManager {

    @Inject
    private final SqlSessionFactory sqlSessionFactory;


    public TransactionManager(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }


    public <R> R runInTransaction(@NonNull Function<SqlSession, R> runnable) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession(false)) {
            try {
                /*
                TODO sqlSession has to be carried over for the time being unless there is a way to bind it to the current request
                 Ideally the transaction manager should know if there is already a transaction in progress and use an existing one
                 */
                R result = runnable.apply(sqlSession);
                sqlSession.commit();
                return result;

            } catch (Exception e) {
                log.error("Error occurred while running transaction.", e);
                sqlSession.rollback();
                throw new DataAccessException("Unable to process runInTransaction request", e);
            }
        }

    }
}
