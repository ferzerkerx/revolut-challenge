package revolut.domain;

import revolut.domain.dto.AccountDto;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;

import javax.inject.Inject;

@Controller("/v1/accounts/")
public class AccountController {
    @Inject
    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @Get("/{accountId}")
    public HttpResponse<AccountDto> accountDetails(@PathVariable String accountId) {
        Account account = accountService.accountDetails(accountId);
        return HttpResponse.ok(AccountDto.of(account));
    }
}
