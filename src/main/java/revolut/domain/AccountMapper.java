package revolut.domain;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.math.BigDecimal;

public interface AccountMapper {

    @Select("SELECT * FROM account WHERE account_id=#{id}")
    Account findById(String accountId);

    @Select("SELECT * FROM account WHERE account_id=#{id} FOR UPDATE")
    Account findAccountForUpdate(String accountId);

    @Update("UPDATE account SET amount=#{amount} WHERE id=#{id}")
    void update(@Param("id") Long id, @Param("amount") BigDecimal amount);
}
