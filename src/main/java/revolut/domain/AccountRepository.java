package revolut.domain;

import lombok.NonNull;
import org.apache.ibatis.session.SqlSession;

import javax.inject.Singleton;

@Singleton
public class AccountRepository {

    public Account findAccountForUpdate(@NonNull SqlSession sqlSession, @NonNull String accountId) {
        return getAccountMapper(sqlSession).findAccountForUpdate(accountId);
    }

    public Account findAccount(@NonNull SqlSession sqlSession, @NonNull String accountId) {
        return getAccountMapper(sqlSession).findById(accountId);
    }

    public void update(@NonNull SqlSession sqlSession, @NonNull Account account) {
        getAccountMapper(sqlSession).update(account.getId(), account.getAmount());
    }

    private AccountMapper getAccountMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(AccountMapper.class);
    }
}
