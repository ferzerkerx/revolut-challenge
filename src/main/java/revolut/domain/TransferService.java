package revolut.domain;

import revolut.config.TransactionManager;
import revolut.domain.accounting.LedgerEntry;
import revolut.domain.accounting.LedgerRepository;
import revolut.domain.command.TransferMoneyCommand;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.javamoney.moneta.Money;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Singleton
@Slf4j
public class TransferService {

    @Inject
    private final AccountRepository accountRepository;
    @Inject
    private final LedgerRepository ledgerRepository;
    @Inject
    private final TransactionManager transactionManager;


    public TransferService(AccountRepository accountRepository, LedgerRepository ledgerRepository, TransactionManager transactionManager) {
        this.accountRepository = accountRepository;
        this.ledgerRepository = ledgerRepository;
        this.transactionManager = transactionManager;
    }

    public TransferResult transfer(@NonNull TransferMoneyCommand transferMoneyCommand) {
        return transactionManager.runInTransaction((sqlSession) -> {

            //TODO check no similar operation has been triggered recently
            Account sourceAccount = accountRepository.findAccountForUpdate(sqlSession, transferMoneyCommand.getSourceAccount());
            Account destinationAccount = accountRepository.findAccountForUpdate(sqlSession, transferMoneyCommand.getDestinationAccount());

            Money targetMoneyAmount = transferMoneyCommand.getMoneyAmount();
            Account modifiedSourceAccount = sourceAccount.withDrawFunds(targetMoneyAmount);
            Account modifiedDestinationAccount = destinationAccount.addFunds(targetMoneyAmount);

            accountRepository.update(sqlSession, modifiedSourceAccount);
            accountRepository.update(sqlSession, modifiedDestinationAccount);

            String transactionId = generateTransactionId().toString();

            ledgerRepository.save(sqlSession, toLedgerEntries(transferMoneyCommand, transactionId));

            TransferResult transferResult = TransferResult.builder()
                    .transactionId(transactionId)
                    .build();
            log.info("TransferMoneyCommand: {} successfully applied with result: {}", transferMoneyCommand, transferResult);
            return transferResult;
        });
    }

    private static UUID generateTransactionId() {
        return UUID.randomUUID();
    }

    static List<LedgerEntry> toLedgerEntries(@NonNull TransferMoneyCommand transferMoneyCommand, String transactionId) {
        LedgerEntry debitEntry = LedgerEntry.debitLedgerEntry()
                .accountId(transferMoneyCommand.getDestinationAccount())
                .reference(transferMoneyCommand.getSourceAccount())
                .transactionId(transactionId)
                .amount(transferMoneyCommand.getAmount())
                .currencyCode(transferMoneyCommand.getCurrencyCode())
                .build();


        LedgerEntry creditEntry = LedgerEntry.creditLedgerEntry()
                .accountId(transferMoneyCommand.getSourceAccount())
                .reference(transferMoneyCommand.getDestinationAccount())
                .amount(transferMoneyCommand.getAmount())
                .currencyCode(transferMoneyCommand.getCurrencyCode())
                .transactionId(transactionId)
                .build();

        return Arrays.asList(debitEntry, creditEntry);
    }


}