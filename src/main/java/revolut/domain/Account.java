package revolut.domain;

import revolut.domain.exception.BadRequestException;
import revolut.domain.exception.InsufficientFundsException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.javamoney.moneta.Money;

import java.math.BigDecimal;
import java.util.Objects;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
public class Account {

    private Long id;
    private String accountId;
    private BigDecimal amount;
    private String currencyCode;

    public Money getBalance() {
        return Money.of(amount, currencyCode);
    }

    public Account withDrawFunds(Money amount) {
        validateSameCurrency(amount);

        Money newAmount = getBalance().subtract(amount);
        if (newAmount.isNegative()) {
            throw new InsufficientFundsException(String.format("Account %s has insufficient funds for amount: %s", accountId, amount));
        }
        return this.toBuilder()
                .amount(newAmount.getNumberStripped())
                .build();
    }

    public Account addFunds(Money amount) {
        validateSameCurrency(amount);
        Money newAmount = getBalance().add(amount);

        return this.toBuilder()
                .amount(newAmount.getNumberStripped())
                .build();
    }

    private void validateSameCurrency(Money amount) {
        if (!Objects.equals(amount.getCurrency().getCurrencyCode(), currencyCode)) {
            //TODO convert to exchange rate
            throw new BadRequestException("Accounts cannot have different currencies.");
        }
    }
}
