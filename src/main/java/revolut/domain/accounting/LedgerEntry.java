package revolut.domain.accounting;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;
import java.time.Instant;

@Value
@Builder
@AllArgsConstructor
public class LedgerEntry {
    private String accountId;

    private String reference;

    private String transactionId;

    private BigDecimal amount;

    private String currencyCode;

    private String operation;

    private Instant createdAt;

    public static LedgerEntry.LedgerEntryBuilder debitLedgerEntry() {
        return LedgerEntry.builder()
                .createdAt(Instant.now())
                .operation("DEBIT");
    }

    public static LedgerEntry.LedgerEntryBuilder creditLedgerEntry() {
        return LedgerEntry.builder()
                .createdAt(Instant.now())
                .operation("CREDIT");
    }
}
