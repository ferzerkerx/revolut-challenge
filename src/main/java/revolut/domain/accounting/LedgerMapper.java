package revolut.domain.accounting;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;

import java.math.BigDecimal;
import java.time.Instant;

public interface LedgerMapper {

    //TODO maybe using dot notation on the root object is better? obj.prop1
    @Insert("INSERT INTO ledger (account_id, reference, transaction_id, amount, currency_code, operation, created_at)" +
            " VALUES (#{accountId}, #{reference}, #{transactionId}, #{amount}, #{currencyCode}, #{operation}, #{createdAt}) ")
    void save(SqlSession sqlSession,
              @Param("accountId") String accountId,
              @Param("reference") String reference,
              @Param("transactionId") String transactionId,
              @Param("amount") BigDecimal amount,
              @Param("currencyCode") String currencyCode,
              @Param("operation") String operation,
              @Param("createdAt") Instant createdAt
    );
}
