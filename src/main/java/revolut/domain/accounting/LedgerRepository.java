package revolut.domain.accounting;

import lombok.NonNull;
import org.apache.ibatis.session.SqlSession;

import javax.inject.Singleton;
import java.util.List;

@Singleton
public class LedgerRepository {

    public void save(@NonNull SqlSession sqlSession, @NonNull List<LedgerEntry> ledgerEntries) {
        for (LedgerEntry ledgerEntry : ledgerEntries) {
            getLedgerMapper(sqlSession).save(
                    sqlSession,
                    ledgerEntry.getAccountId(),
                    ledgerEntry.getReference(),
                    ledgerEntry.getTransactionId(),
                    ledgerEntry.getAmount(),
                    ledgerEntry.getCurrencyCode(),
                    ledgerEntry.getOperation(),
                    ledgerEntry.getCreatedAt()
            );
        }
    }

    private LedgerMapper getLedgerMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(LedgerMapper.class);
    }

}
