package revolut.domain.exception;

import lombok.NonNull;

public class InsufficientFundsException extends RuntimeException {
    public InsufficientFundsException(@NonNull String reason, @NonNull Exception causedBy) {
        super(reason, causedBy);
    }

    public InsufficientFundsException(@NonNull String reason) {
        super(reason);
    }
}
