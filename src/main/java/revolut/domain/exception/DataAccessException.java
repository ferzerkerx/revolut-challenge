package revolut.domain.exception;

import lombok.NonNull;

public class DataAccessException extends RuntimeException {
    public DataAccessException(@NonNull String reason, @NonNull Exception causedBy) {
        super(reason, causedBy);
    }
}
