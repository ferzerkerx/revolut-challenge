package revolut.domain.exception;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;

@Produces
@Singleton
@Slf4j
public class BadRequestExceptionHandler implements ExceptionHandler<BadRequestException, HttpResponse> {

    @Override
    public HttpResponse handle(HttpRequest request, BadRequestException exception) {
        log.warn("BadRequestException", exception);
        return HttpResponse.badRequest(exception.getMessage());
    }
}
