package revolut.domain.exception;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;

@Produces
@Singleton
@Slf4j
public class DataAccessExceptionHandler implements ExceptionHandler<DataAccessException, HttpResponse> {
    @Override
    public HttpResponse handle(HttpRequest request, DataAccessException exception) {
        log.error("InsufficientFundsException", exception);
        return HttpResponse.serverError();
    }
}
