package revolut.domain.exception;

import lombok.NonNull;

public class BadRequestException extends RuntimeException {
    public BadRequestException(@NonNull String reason) {
        super(reason);
    }


}
