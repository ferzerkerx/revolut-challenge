package revolut.domain.exception;

import lombok.NonNull;

public class AccountNotFoundException extends RuntimeException {
    public AccountNotFoundException(@NonNull String reason) {
        super(reason);
    }
}
