package revolut.domain;

import revolut.domain.exception.AccountNotFoundException;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Optional;

@Singleton
public class AccountService {

    @Inject
    private final SqlSessionFactory sqlSessionFactory;
    @Inject
    private final AccountRepository accountRepository;


    public AccountService(SqlSessionFactory sqlSessionFactory, AccountRepository accountRepository) {
        this.sqlSessionFactory = sqlSessionFactory;
        this.accountRepository = accountRepository;
    }

    public Account accountDetails(String accountId) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            Account account = accountRepository.findAccount(sqlSession, accountId);
            return Optional.ofNullable(account)
                    .orElseThrow(() -> new AccountNotFoundException("account not found:" + accountId));
        }
    }
}
