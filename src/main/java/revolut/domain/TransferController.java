package revolut.domain;

import revolut.domain.command.TransferMoneyCommand;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.micronaut.validation.Validated;

import javax.inject.Inject;
import javax.validation.Valid;

@Controller("/v1/transfer")
@Validated
public class TransferController {

    @Inject
    private final TransferService transferService;

    public TransferController(TransferService transferService) {
        this.transferService = transferService;
    }

    @Post
    public HttpResponse<TransferResult> transfer(@Body @Valid TransferMoneyCommand transferMoneyCommand) {
        TransferResult transferResult = transferService.transfer(transferMoneyCommand);
        return HttpResponse.ok(transferResult);
    }
}