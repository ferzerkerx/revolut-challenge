package revolut.domain.dto;

import revolut.domain.Account;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder
@AllArgsConstructor
public class AccountDto {
    private String accountId;
    private BigDecimal amount;
    private String currencyCode;

    public static AccountDto of(Account account) {
        return AccountDto.builder()
                .accountId(account.getAccountId())
                .amount(account.getAmount())
                .currencyCode(account.getCurrencyCode())
                .build();
    }
}
